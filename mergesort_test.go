package main

import (
	"reflect"
	"testing"
)

func TestMergesort(t *testing.T) {
	s := []int{5, 8, 9, 5, 0, 10, 1, 6}
	expected := []int{0, 1, 5, 5, 6, 8, 9, 10}
	s = mergeSort(s)
	if !reflect.DeepEqual(expected, s) {
		t.Error("result is not as expected. Expect", expected, "but got", s)
	}
}
