package main

import (
	"fmt"
)

func merge(left []int, right []int) []int {
	var i, j int
	lenLeft := len(left)
	lenRight := len(right)
	result := []int{}

	for i < lenLeft || j < lenRight {
		if i < lenLeft && j < lenRight {
			if left[i] < right[j] {
				result = append(result, left[i])
				i = i + 1
			} else {
				result = append(result, right[j])
				j = j + 1
			}
		}
		if i >= lenLeft {
			for _, x := range right[j:] {
				result = append(result, x)
			}
			j = lenRight
		}
		if j >= lenRight {
			for _, x := range left[i:] {
				result = append(result, x)
			}
			i = lenLeft
		}
	}
	return result
}

func mergeSort(input []int) []int {
	if len(input) == 1 {
		return input
	}
	middle := len(input) / 2
	left := mergeSort(input[0:middle])
	right := mergeSort(input[middle:])
	return merge(left, right)
}

func main() {
	input := []int{5, 8, 9, 5, 0, 10, 1, 6}
	fmt.Println("result: ", mergeSort(input))
}
